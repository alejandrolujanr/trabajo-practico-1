require('dotenv').config();
const express = require('express');
const morgan = require('morgan');

const app = express();

const rutasUsuarios = require('./routs/usuarios');
const rutasshoppings = require('./routs/shoppings');
const rutasComercios = require('./routs/comercios');


app.use(express.text());
app.use(express.json());

app.use(morgan('tiny'));

app.use('/usuarios', rutasUsuarios);
app.use('/shoppings', rutasshoppings);
app.use('/comercios', rutasComercios);

app.listen(process.env.PORT, () => {
    console.log("Consola corriendo en el puerto 3000");
})