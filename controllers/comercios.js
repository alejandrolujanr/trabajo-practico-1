const getComercio = (req, res) => {
    res.status(200).send("El Comercio se muestra OK");
};

const postComercio = (req, res) => {
    res.status(200).send("Comercio cargado OK");
};

const putComercio = (req, res) => {
    res.status(200).send("Comercio modificado OK");
};

const deleteComercio = (req, res) => {
    res.status(200).send("Comercio borrado OK");
};

module.exports = {
    getComercio,
    postComercio,
    putComercio,
    deleteComercio
}