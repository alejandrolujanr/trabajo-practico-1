const getUsuario = (req, res) => {
    res.status(200).send("Usuario buscado OK");
}

const postUsuario = (req, res) => {
    res.status(200).send("Usuario creado OK");
}

const putUsuario = (req, res) => {
    res.status(200).send("Usuario editado OK");
}

const deleteUsuario = (req, res) => {
    res.status(200).send("Usuario borrado OK");
}

module.exports = {
    getUsuario,
    postUsuario,
    putUsuario,
    deleteUsuario
};