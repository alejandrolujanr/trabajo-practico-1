const getShopping = (req, res) => {
    res.status(200).send("Shopping mostrado OK");
}

const postShopping = (req, res) => {
    res.status(200).send("Shopping creado OK");
}

const putShopping = (req, res) => {
    res.status(200).send("Shopping editado OK");
}

const deleteShopping = (req, res) => {
    res.status(200).send("Shopping borrado OK");
}

module.exports = {
    getShopping,
    postShopping,
    putShopping,
    deleteShopping
}