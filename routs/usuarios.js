const { Router } = require('express');
const router = Router();

const {
    getUsuario,
    postUsuario,
    putUsuario,
    deleteUsuario
} = require('../controllers/usuarios')

const { validarIdUsuario, validarPostUsuario, validarPutUsuario } = require('../middlewares/validarUsuarios');

router.get('/:id', validarIdUsuario, getUsuario)
router.post('/', validarPostUsuario, postUsuario)
router.put('/:id', validarIdUsuario, validarPutUsuario, putUsuario)
router.delete('/:id', validarIdUsuario, deleteUsuario)

module.exports = router;