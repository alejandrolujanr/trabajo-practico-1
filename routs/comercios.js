const express = require('express');
const router = express.Router();

const {
    getComercio,
    postComercio,
    putComercio,
    deleteComercio
} = require('../controllers/comercios')

const { validarIdParametroComercio, validarPostComercio, validarPutComercio } = require('../middlewares/validarComercios');

router.get('/:id', validarIdParametroComercio, getComercio)
router.post('/', validarPostComercio, postComercio)
router.put('/:id', validarIdParametroComercio, validarPutComercio, putComercio)
router.delete('/:id', deleteComercio)

module.exports = router;