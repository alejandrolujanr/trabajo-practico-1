const { Router } = require('express');
const router = Router();

const {
    getShopping,
    postShopping,
    putShopping,
    deleteShopping
} = require('../controllers/shoppings')

const { validarPostShopping, validarIdShopping, validarPutShopping } = require('../middlewares/validarShopping');

router.get('/:id', validarIdShopping, getShopping)
router.post('/', validarPostShopping, postShopping)
router.put('/:id', validarIdShopping, validarPutShopping, putShopping)
router.delete('/:id', validarIdShopping, deleteShopping)

module.exports = router;