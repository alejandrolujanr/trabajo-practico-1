const Joi = require('joi');
validarIdUsuario = async(req, res, next) => {
    const schema = Joi.object({
        id: Joi.string().required()
    }).required();

    try {
        await schema.validateAsync(req.params);
        return next();
    } catch (error) {
        return res.status(400).send({
            mensaje: "Datos de entrada para búsqueda de usuarios incorrectos"
        })
    }
};

validarPostUsuario = async(req, res, next) => {
    const schema = Joi.object({
        id: Joi.string().required(),
        nombre: Joi.string().required(),
        apellido: Joi.string().required(),
        fec_nac: Joi.date().required(),
        password: Joi.string().required(),
        email: Joi.string().email().required(),
        roles: Joi.array()
            .items(
                Joi.string().valid(
                    'ADMIN',
                    'SHOPPING_MANAGER',
                    'COMERCE_MANAGER',
                    'USER'
                )
            )
            .max(4)
            .required()
    });

    try {
        await schema.validateAsync(req.body);
        return next();
    } catch (error) {
        console.log(error);
        return res.status(400).send({
            mensaje: "Datos de entrada para creación de usuarios incorrectos"
        })
    }
};

validarPutUsuario = async(req, res, next) => {
    const schema = Joi.object({
        nombre: Joi.string(),
        apellido: Joi.string(),
        fec_nac: Joi.date(),
        password: Joi.string(),
        email: Joi.string().email().email(),
        roles: Joi.array()
            .items(
                Joi.string().valid(
                    'ADMIN',
                    'SHOPPING_MANAGER',
                    'COMERCE_MANAGER',
                    'USER'
                )
            )
            .max(4)
    });

    try {
        await schema.validateAsync(req.body);
        return next();
    } catch (error) {
        return res.status(400).send({
            mensaje: "Datos de entrada para edición de comercios incorrectos"
        })
    }
};

module.exports = {
    validarIdUsuario,
    validarPostUsuario,
    validarPutUsuario
};