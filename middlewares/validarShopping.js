const Joi = require('joi');
const validarIdShopping = async(req, res, next) => {
    const schema = Joi.object({
        id: Joi.string().required()
    }).required();

    try {
        await schema.validateAsync(req.params);
        return next();
    } catch (error) {
        return res.status(400).send({
            mensaje: "Datos de entrada para búsqueda de shoppings incorrectos"
        })
    }
};

const validarPostShopping = async(req, res, next) => {
    const schema = Joi.object({
        id: Joi.string().required(),
        nombre: Joi.string().required(),
        manager: Joi.string().required(),
        comercios: Joi.array()
            .items(
                Joi.string()
            ).required()
    });

    try {
        await schema.validateAsync(req.body);
        next();
    } catch (error) {
        console.log(error);
        return res.status(400).send({
            mensaje: "Datos de entrada para la creación de shoppings incorrectos"
        })
    }
};

const validarPutShopping = async(req, res, next) => {
    const schema = Joi.object({
        nombre: Joi.string().required(),
        manager: Joi.string().required(),
        comercios: Joi.array()
            .items(
                Joi.string()
            ).required()
    });

    try {
        await schema.validateAsync(req.body);
        next();
    } catch (error) {
        return res.status(400).send({
            mensaje: "Datos de entrada para la edición de Shopping incorrectos"
        })
    }
};

module.exports = {
    validarPostShopping,
    validarIdShopping,
    validarPutShopping
};