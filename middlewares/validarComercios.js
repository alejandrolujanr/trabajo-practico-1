const Joi = require('joi');
const validarIdParametroComercio = async(req, res, next) => {
    const schema = Joi.object({
        id: Joi.string().required()
    }).required();

    try {
        await schema.validateAsync(req.params);
        return next();
    } catch (error) {
        return res.status(400).send({
            mensaje: "Datos de entrada para búsqueda de comercios incorrectos"
        })
    }
};

const validarPostComercio = async(req, res, next) => {
    const schema = Joi.object({
        id: Joi.string().required(),
        nombre: Joi.string().required(),
        manager: Joi.string().required(),
        shopping: Joi.string().required()
    });

    try {
        await schema.validateAsync(req.body);
        next();
    } catch (error) {
        console.log(error);
        return res.status(400).send({
            mensaje: "Datos de entrada para creación de comercio incorrectos"
        })
    }
};

const validarPutComercio = async(req, res, next) => {
    const schema = Joi.object({
        nombre: Joi.string().required(),
        manager: Joi.string().required(),
        shopping: Joi.string().required()
    });

    try {
        await schema.validateAsync(req.body);
        next();
    } catch (error) {
        return res.status(400).send({
            mensaje: "Datos de entrada para edición de comercios incorrectos"
        })
    }
};

module.exports = {
    validarIdParametroComercio,
    validarPostComercio,
    validarPutComercio
};